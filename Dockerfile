# Start with latest golang image
FROM golang:alpine as builder
# Select our working directory
WORKDIR /app
# Install the build tools we need
RUN apk add --no-cache make curl git build-base gcc g++ libc-dev binutils-gold

# Clone the project to working directory
RUN git clone https://github.com/devopsfaith/krakend-ce.git .
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

# VERSION is currently based on the last commit
RUN VERSION=$(git describe --tags) \
&& COMMIT=$(git rev-parse HEAD) \
&& BUILD=$(date +%FT%T%z) \
&& LDFLAG_LOCATION=github.com/devopsfaith/krakend/core \
&& LDFLAGS="-ldflags \"-X ${LDFLAG_LOCATION}.KrakendVersion=${VERSION}\""

RUN GOPROXY=https://goproxy.io go get .
RUN GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build ${LDFLAGS} -o krakend ./cmd/krakend-ce

FROM alpine

LABEL maintainer="admin@3pi.dev"

RUN mkdir -p /etc/krakend/
COPY --from=builder /app/krakend /usr/bin/krakend

RUN addgroup -S krakend && adduser -S -G krakend krakend

USER krakend

VOLUME [ "/etc/krakend" ]

ENTRYPOINT [ "/usr/bin/krakend" ]
CMD [ "run", "-c", "/etc/krakend/krakend.json" ]

EXPOSE 8000 8090

