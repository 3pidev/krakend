# Krakend

[Krakend](https://krakend.io) is an opensource API gateway with a small memory 
footprint and high performance. It was not available for ARM64 and therefor we 
build this docker file from source.